import { createReducer, on, Action } from '@ngrx/store';
import * as AuthActions from './auth.actions';
import { IUserData } from '../../interfaces';

export const authFeatureName = 'auth';

export interface AuthState {
  isAuthenticated: boolean;
  user: IUserData | null;
  errorMessage?: string;
}

export const initialAuthState: AuthState = {
  isAuthenticated: false,
  user: null,
  errorMessage: '',
};

const authReducerInternal = createReducer(
  initialAuthState,

  on(AuthActions.LoginRequest, (state) => {
    return {
      ...state,
      user: null,
      isAuthenticated: false,
      errorMessage: '',
    };
  }),

  on(AuthActions.LoginSuccess, (state, action) => {
    return {
      ...state,
      user: action.payload,
      isAuthenticated: true,
      errorMessage: '',
    };
  }),

  on(AuthActions.LoginFailure, (state, action) => {
    return {
      ...state,
      errorMessage: action.payload.message,
    };
  }),

  on(AuthActions.AuthCheckRequest, (state) => {
    return {
      ...state,
      user: null,
      isAuthenticated: false,
      errorMessage: '',
    };
  }),

  on(AuthActions.SignupFailure, (state, action) => {
    return {
      ...state,
      user: null,
      isAuthenticated: false,
      errorMessage: action.payload.message
    };
  }),

  // on(AuthActions.LogoutRequest, (state) => {
  //   return {
  //     user: null,
  //     isAuthenticated: false,
  //     errorMessage: '',
  //   };
  // }),

  on(AuthActions.LogoutSuccess, (state) => {
    return {
      ...state,
      user: null,
      isAuthenticated: false,
      errorMessage: '',
    };
  }),

  on(AuthActions.LogoutFailure, (state, action) => {
    return {
      ...state,
      errorMessage: 'Çıkış yaparken hata ile karşılaşıldı.',
    };
  }),

);

export function authReducer(state: AuthState | undefined, action: Action): AuthState {
  return authReducerInternal(state, action);
}
