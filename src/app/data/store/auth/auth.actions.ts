import { createAction, createFeatureSelector, props } from '@ngrx/store';
import { IResponse, IUserData, IUserLogin, IUserRegister, IUserResponse } from 'src/app/data/interfaces';
import { IResponseBase } from '../../interfaces/response.interface';
import { AuthState } from './auth.reducer';

// export const Login = createAction('[Auth] Login', props<{payload: IUserLogin}>());

export const selectAuthState = createFeatureSelector<AuthState>('auth');

/* + */
export const LoginRequest = createAction('[Auth] Login Request', props<{ payload: IUserLogin }>());
/* + */
export const LoginSuccess = createAction('[Auth] Login Success', props<{ payload: IUserResponse }>());
/* + */
export const LoginFailure = createAction('[Auth] Login Failure', props<{ payload: IResponseBase }>());

/* - */
export const AuthCheckRequest = createAction('[Auth] Auth Check Request', props<{ payload: IUserResponse }>());
/* - */
export const AuthCheckSuccess = createAction('[Auth] Auth Check Success', props<{ payload: IUserResponse }>());
/* - */
export const AuthCheckFailure = createAction('[Auth] Auth Check Failure', props<{ payload: IResponseBase }>());

/* + */
export const LogoutRequest = createAction('[Auth] Logout Request');
/* + */
export const LogoutSuccess = createAction('[Auth] Logout Success');
/* + */
export const LogoutFailure = createAction('[Auth] Logout Failure', props<{ payload: IResponseBase }>());

/* + */
export const SignupRequest = createAction('[Auth] Signup Request', props<{ payload: IUserRegister }>());
/*  */
export const SignupSuccess = createAction('[Auth] Signup Success', props<{ payload: IUserResponse }>());
/* + */
export const SignupFailure = createAction('[Auth] Signup Failure', props<{ payload: IResponseBase }>());


// export const decrement = createAction('[Auth] Decrement');
// export const reset = createAction('[Auth] Reset');

/*

export const checkAuth = createAction('[Auth] checkAuth');
export const checkAuthComplete = createAction(
  '[Auth] checkAuthComplete',
  props<{ isLoggedIn: boolean }>()
);
export const login = createAction('[Auth] login');
export const loginComplete = createAction(
  '[Auth] loginComplete',
  props<{ profile: any; isLoggedIn: boolean }>()
);
export const logout = createAction('[Auth] logout');
export const logoutComplete = createAction('[Auth] logoutComplete');

*/
