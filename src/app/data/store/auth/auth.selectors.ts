import { createFeatureSelector, createSelector } from '@ngrx/store';
import { AuthState, authFeatureName } from './auth.reducer';

export const getAuthFeatureState = createFeatureSelector<AuthState>(authFeatureName);

// export const selectIsAuthenticated = createSelector(
//   getAuthFeatureState,
//   (state: AuthState) => state.isAuthenticated
// );

// export const selectuserInfo = createSelector(
//   getAuthFeatureState,
//   (state: AuthState) => state.user
// );

export const selectAuthUser = (state: AuthState) => state.user;
