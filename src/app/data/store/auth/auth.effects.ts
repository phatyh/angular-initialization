import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType, OnInitEffects } from '@ngrx/effects';
import { catchError, exhaustMap, filter, map, switchMap, tap } from 'rxjs/operators';
import { Action } from '@ngrx/store';
import { from, of } from 'rxjs';
import * as AuthActions from './auth.actions';
import { AuthService } from '../../services/auth.service';
import { IResponseBase } from '../../interfaces/response.interface';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LogoutComponent } from 'src/app/modules/auth/logout/logout.component';

@Injectable()
export class AuthEffects implements OnInitEffects {

  loginRequest$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.LoginRequest),
      map(action => action.payload),
      exhaustMap(action => {
        return this.authService.login(action).pipe(
          // map(user => user.data),
          map(user => AuthActions.LoginSuccess({ payload: user.data })),
          catchError((error) => {
            const getError: IResponseBase = error.error;

            return of(AuthActions.LoginFailure({ payload: getError }));
          }),
        );
      })
    )
  );

  loginSuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.LoginSuccess),
      tap(result => {
        this.authService.loginSuccess(result.payload);
      }),
    ),
    { dispatch: false }
  );

  // loginFailure$ = createEffect(() =>
  //   this.actions$.pipe(
  //     ofType(AuthActions.LoginFailure),
  //   ),
  //   { dispatch: false }
  // );

  authCheckRequest$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.AuthCheckRequest),
      map(action => action.payload),
      filter(() => !!this.authService.token),
      exhaustMap(() => {
        return this.authService.me().pipe(
          map(user => {
            user.data.token = this.authService.token;
            return user.data;
          }),
          map(user => AuthActions.AuthCheckSuccess({ payload: user })),
          catchError((error) => {
            const getError: IResponseBase = error.error;

            return of(AuthActions.AuthCheckFailure({ payload: getError }));
          }),
        );
      })
    )
  );

  signupRequest$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.SignupRequest),
      map(result => result.payload),
      exhaustMap(result => {
        return this.authService.register(result).pipe(
          // tap(user => console.log(user)),
          // map(user => user.data),
          map(user => AuthActions.SignupSuccess({ payload: user.data })),
          catchError((error) => {
            const getError: IResponseBase = error.error;

            return of(AuthActions.SignupFailure({ payload: getError }));
          }),
        );
      })
    ),
  );

  // signupSuccess$ = createEffect(() =>
  //   this.actions$.pipe(
  //     ofType(AuthActions.SignupSuccess),
  //     tap(result => {
  //       // this.authService.loginSuccess(result.payload);
  //     }),
  //   ),
  //   { dispatch: false }
  // );

  // signupFailure$ = createEffect(() =>
  //   this.actions$.pipe(
  //     ofType(AuthActions.SignupFailure),
  //   ),
  //   { dispatch: false }
  // );


  logoutRequest$ = createEffect(() => this.actions$.pipe(
    ofType(AuthActions.LogoutRequest),
    exhaustMap(() => this.dialog(LogoutComponent).pipe(
      map(() => AuthActions.LogoutSuccess()),
      catchError(error => of(AuthActions.LogoutFailure({ payload: error })))
    ))
  ));

  logoutSuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.LogoutSuccess),
      tap(() => console.log('ok')),
      tap(() => this.authService.logout()),
      // map(() => fromAuthActions.logoutComplete())
    ),
    { dispatch: false }
  );

  dialog = (content: any) => {
    const modalRef = this.modal.open(content);

    return from(modalRef.result);
  }

  constructor(
    private actions$: Actions,
    private authService: AuthService,
    private router: Router,
    private modal: NgbModal
  ) { }

  ngrxOnInitEffects(): Action {
    return { type: '[Auth] Auth Check Request' };
  }

  //   login$ = createEffect(() => this.actions$.pipe(
  //     ofType(AuthActions.Login),
  //     // map((action) => action.payload),
  //     exhaustMap(action =>
  //         this.authService.login(action.payload).pipe(
  //           map(user => AuthApiActions.loginSuccess({ user })),
  //           catchError(error => of(AuthApiActions.loginFailure({ error })))
  //         )
  //       )
  //     )
  //   );


  /*
    Error:  {"headers":{"normalizedNames":{},"lazyUpdate":null},"status":400,"statusText":"Bad Request",
    "url":"http://localhost:3000/api/auth/login","ok":false,"name":"HttpErrorResponse",
    "message":"Http failure response for http://localhost:3000/api/auth/login: 400 Bad Request",
    "error":{"message":"Şifre veya Email yanlış","status":false,"data":"","errorCode":400}}
    login$ = createEffect(
    () =>
      this.actions$.pipe(
      ofType(fromAuthActions.login),
      switchMap(() => this.authService.doLogin())
      ),
    { dispatch: false }
    );

    checkauth$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromAuthActions.checkAuth),
      switchMap(() =>
      this.authService
        .checkAuth()
        .pipe(
        map((isLoggedIn) =>
          fromAuthActions.checkAuthComplete({ isLoggedIn })
        )
        )
      )
    )
    );

    checkAuthComplete$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromAuthActions.checkAuthComplete),
      switchMap(({ isLoggedIn }) => {
      if (isLoggedIn) {
        return this.authService.userData.pipe(
        map((profile) =>
          fromAuthActions.loginComplete({ profile, isLoggedIn })
        )
        );
      }
      return of(fromAuthActions.logoutComplete());
      })
    )
    );

    logout$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromAuthActions.logout),
      tap(() => this.authService.logout()),
      map(() => fromAuthActions.logoutComplete())
    )
    );

    logoutComplete$ = createEffect(
    () =>
      this.actions$.pipe(
      ofType(fromAuthActions.logoutComplete),
      tap(() => {
        this.router.navigate(['/']);
      })
      ),
    { dispatch: false }
    );*/
}
