export interface IChoices {
  hash: string;
  questionHash: string;
  answer: string;
}
