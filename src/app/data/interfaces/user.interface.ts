export interface IUserLogin {
  email: string;
  password: string;
  remember: boolean;
}

export interface IUserRegister {
  email: string;
  password: string;
  passwordVerify?: string;
  firstName: string;
  lastName: string;
  acceptTerms: boolean;
}

export interface IUserData {
  id: number;
  email: string;
  firstName: string;
  lastName: string;
}

export interface IUserResponse extends IUserData {
  token: string;
}

export interface IUserList {
  users: IUserData[];
}
