export interface IResponse<T> extends IResponseBase {
  data: T;
}

export interface IResponseBase {
  status: boolean;
  message?: string;
  errorCode?: number;
}
