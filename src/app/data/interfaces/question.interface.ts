import { IChoices } from './choice.interface';

export interface IQuestion {
  hash?: string;
  question: string;
  correct?: string;
  createdAt?: Date;
  createdUser?: number;
  updatedAt?: Date;
  choices?: IChoices[];
}

export interface IQuestionCheck {
  question: string;
  answer: string;
}
