import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, ActivatedRoute } from '@angular/router';
import { Injectable } from '@angular/core';
import { AuthService } from '../services/auth.service';

@Injectable()
export class SessionGuard implements CanActivate {
  constructor(
    private router: Router,
    private authService: AuthService,
    private route: ActivatedRoute,
  ) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): boolean {
    const isLoggedIn = this.authService.isLogged();

    console.log(isLoggedIn);


    if (isLoggedIn) {
      const returnUrl: string = this.route.snapshot.queryParamMap.get('return-url') || '/';

      this.router.navigate([returnUrl]);
    }

    return !isLoggedIn;
  }
}
