import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { IResponse } from 'src/app/data/interfaces';

@Injectable({
  providedIn: 'root'
})
export class BaseService<T> {
  suffix: string = '';
  requestBase: string = `${environment.apiBase}${environment.prefix}`;

  constructor(
    protected http: HttpClient,
  ) {}

  create(data: T): Observable<IResponse<T>> {
    return this.http.post<IResponse<T>>(`${this.requestBase}/${this.suffix}`, data, {headers: this.setHeaders});
  }

  read(id: string | number): Observable<IResponse<T>> {
    return this.http.get<IResponse<T>>(`${this.requestBase}/${this.suffix}/${id}`);
  }

  list(): Observable<IResponse<T[]>> {
    return this.http.get<IResponse<T[]>>(`${this.requestBase}/${this.suffix}`);
  }

  update(data: T): Observable<IResponse<T>> {
    return this.http.patch<IResponse<T>>(`${this.requestBase}/${this.suffix}`, data, {headers: this.setHeaders});
  }

  delete(id: string | number): Observable<IResponse<T>> {
    return this.http.delete<IResponse<T>>(`${this.requestBase}/${this.suffix}/${id}`, {headers: this.setHeaders});
  }

  protected get setHeaders(): HttpHeaders {
    let setHeaders = new HttpHeaders();
    setHeaders = setHeaders.append('Content-Type', 'application/json');

    return setHeaders;
  }

  /**
   * @todo
   */

  // setParams(): HttpParams {
  //   return new HttpParams();
  // }
}
