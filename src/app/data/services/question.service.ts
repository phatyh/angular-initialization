import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IQuestion, IQuestionCheck, IResponseBase } from 'src/app/data/interfaces';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root'
})
export class QuestionService extends BaseService<IQuestion> {
  suffix: string = 'questions';

  checkAnswer(data: IQuestionCheck): Observable<IResponseBase> {
    return this.http.post<IResponseBase>(`${this.requestBase}/${this.suffix}/check`, data, {
      headers: this.setHeaders
    });
  }

}
