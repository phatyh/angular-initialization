import { Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { tap, map, finalize, timeout } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';
import { environment } from 'src/environments/environment';
import { IUserLogin, IResponse, IUserResponse, IUserData, IUserRegister } from 'src/app/data/interfaces';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  tokenName: string = environment.tokenKey;
  decodedToken: any;
  requestBase: string = `${environment.apiBase}${environment.prefix}`;
  private refreshTokenTimeout: any = null;

  constructor(
    private http: HttpClient,
    private router: Router,
    private jwtHelper: JwtHelperService,
    private route: ActivatedRoute,
  ) { }

  get token(): string {
    return localStorage.getItem(this.tokenName) || '';
  }

  login(data: IUserLogin): Observable<IResponse<IUserResponse>> {
    return this.http.post<IResponse<IUserResponse>>(`${this.requestBase}/auth/login`, data);
  }

  register(data: IUserRegister): Observable<IResponse<IUserResponse>> {
    return this.http.post<IResponse<IUserResponse>>(`${this.requestBase}/auth/register`, data);
  }

  me(): Observable<IResponse<IUserResponse>> {
    return this.http.post<IResponse<IUserResponse>>(`${this.requestBase}/auth/me`, {});
  }

  logout(): void {
    localStorage.removeItem(this.tokenName);

    this.stopRefreshTokenTimer();

    this.router.navigate(['/']);
  }

  saveToken(token: string): void {
    if (token !== undefined) {
      localStorage.setItem(this.tokenName, token);
    }
  }

  isLogged(): boolean {
    return this.jwtHelper.isTokenExpired(this.token) === false;
  }

  decodeToken(): any {
    return this.jwtHelper.decodeToken(this.token);
  }

  getTokenExpirationDate(): Date | null {
    return this.jwtHelper.getTokenExpirationDate(this.token);
  }

  refreshToken(): Observable<any> {
    return this.http.post<any>(`${this.requestBase}/auth/refresh-token`, {}, { withCredentials: true })
      .pipe(map((user) => {
        if (user) {
          this.startRefreshTokenTimer();
          return user;
        }
      }));
  }

  private startRefreshTokenTimer(): void {
    // parse json object from base64 encoded jwt token
    const jwtToken = this.decodeToken();

    // set a timeout to refresh the token a minute before it expires
    const expires = new Date(jwtToken.exp * 1000);
    const getTimeout = expires.getTime() - Date.now() - (60 * 1000);
    console.log(expires.getTime(), getTimeout);

    this.refreshTokenTimeout = setTimeout(() => this.refreshToken().subscribe(), getTimeout);
  }

  private stopRefreshTokenTimer(): void {
    clearTimeout(this.refreshTokenTimeout);
  }

  loginSuccess(result: IUserResponse): void {
    this.saveToken(result.token);

    this.startRefreshTokenTimer();

    const returnUrl: string = this.route.snapshot.queryParamMap.get('return-url') || '/';

    this.router.navigate([returnUrl]);
  }
}
