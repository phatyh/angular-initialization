import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { CustomValidators } from 'ngx-custom-validators';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { IUserLogin } from 'src/app/data/interfaces';
import { AuthService } from 'src/app/data/services';
import { AuthState, LoginRequest, selectAuthState } from 'src/app/data/store/auth';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  returnUrl: string = '';
  loginForm: FormGroup = this.createLoginForm();
  buttonDisabled: boolean = false;
  isSubmit: boolean = false;
  getState: Observable<AuthState>;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private store: Store<AuthState>,
  ) {
    this.getState = this.store.select(selectAuthState);
  }

  ngOnInit(): void {
    // this.getState.subscribe((state) => {
    //   if (state.isAuthenticated) {
    //     this.router.navigate(['/']);
    //   }
    // });
  }

  createLoginForm(): FormGroup {
    return this.formBuilder.group({
      email: ['', [
        Validators.required,
        CustomValidators.email,
      ]],
      password: ['', [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(16)
      ]],
      remember: [true],
    });
  }

  get f(): { [key: string]: AbstractControl; } { return this.loginForm.controls; }

  onLogin(): void {
    this.isSubmit = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    const user: IUserLogin = this.loginForm.value;

    this.store.dispatch(LoginRequest({ payload: user }));
  }

}
