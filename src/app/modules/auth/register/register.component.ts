import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { CustomValidators } from 'ngx-custom-validators';
import { Observable } from 'rxjs';
import { IUserRegister } from 'src/app/data/interfaces';
import { AuthState, selectAuthState, SignupRequest } from 'src/app/data/store/auth';
import Validation from 'src/app/shared/validator/password.validator';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup = this.createRegisterForm();
  isSubmit: boolean = false;
  getState: Observable<AuthState>;

  constructor(
    private formBuilder: FormBuilder,
    private store: Store<AuthState>,
  ) {
    this.getState = this.store.select(selectAuthState);
  }

  ngOnInit(): void {
    this.getState.subscribe(result => {
      console.log(result);

    });
  }

  createRegisterForm(): FormGroup {
    return this.formBuilder.group({
      email: ['', [
        Validators.required,
        CustomValidators.email,
      ]],
      firstname: ['', [Validators.required]],
      lastname: ['', [Validators.required]],
      password: ['', [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(16),
      ]],
      passwordVerify: ['', [
        Validators.required,
      ]],
      acceptTerms: [false, [
        Validators.requiredTrue,
      ]],
    },
    {
      validators: [Validation.mismatch('password', 'passwordVerify')]
    });
  }

  get f(): { [key: string]: AbstractControl; } { return this.registerForm.controls; }

  onRegister(): void {
    this.isSubmit = true;

    console.log(this.registerForm);

    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }

    const user: IUserRegister = this.registerForm.value;

    delete user.passwordVerify;

    this.store.dispatch(SignupRequest({ payload: user }));
  }
}
