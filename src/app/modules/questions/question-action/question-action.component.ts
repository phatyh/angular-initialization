import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IQuestion } from 'src/app/data/interfaces';
import { QuestionService } from 'src/app/data/services';

@Component({
  selector: 'app-question-action',
  templateUrl: './question-action.component.html',
  styleUrls: ['./question-action.component.scss']
})
export class QuestionActionComponent implements OnInit {
  baseForm: FormGroup = this.createForm();

  constructor(
    private formBuilder: FormBuilder,
    private questionService: QuestionService,
  ) { }

  ngOnInit(): void {
  }

  createForm(): FormGroup {
    return this.formBuilder.group({
      question: ['', [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(255),
      ]],
      correct : ['', [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(255),
      ]],
      choices: this.formBuilder.array([
        this.createChoice()
      ]),
    });
  }

  get f(): { [key: string]: AbstractControl; } { return this.baseForm.controls; }

  onSubmit(): void {
    // stop here if form is invalid
    if (this.baseForm.invalid) {
      return;
    }

    const data: IQuestion = this.baseForm.value;

    this.questionService.create(data).subscribe(result => {
      console.log(result);
    });

  }

  get choices(): FormArray {
    return this.baseForm.get('choices') as FormArray;
  }

  createChoice(): FormGroup {
    return this.formBuilder.group({
      answer: ['', [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(255)
      ]]
    });
  }

  onAddChoice(): void {
    this.choices.push(this.createChoice());
  }

  onRemoveChoice(i: number): void {
    this.choices.removeAt(i);
  }
}
