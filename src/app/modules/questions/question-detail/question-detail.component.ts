import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { IChoices, IQuestion } from 'src/app/data/interfaces';
import { QuestionService } from 'src/app/data/services';

@Component({
  selector: 'app-question-detail',
  templateUrl: './question-detail.component.html',
  styleUrls: ['./question-detail.component.scss']
})
export class QuestionDetailComponent implements OnInit {
  hash: string = this.route.snapshot.params.hash;
  question$: Observable<IQuestion> = this.questionService.read(this.hash).pipe(
    map(result => result.data),
  );

  constructor(
    private route: ActivatedRoute,
    private questionService: QuestionService,
  ) { }

  ngOnInit(): void {
    // .subscribe(question => {
    //   console.log(question);

    // });
  }

  onAnswer(choice: IChoices): void {
    console.log(choice, this.hash);

    this.questionService.checkAnswer({
      question: this.hash,
      answer: choice.hash
    }).subscribe(result => {
      console.log(result);

    });
  }

}
