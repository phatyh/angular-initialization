import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { QuestionDetailComponent } from './question-detail/question-detail.component';
import { QuestionActionComponent } from './question-action/question-action.component';

const routes: Routes = [
  {
    path: 'add',
    component: QuestionActionComponent,
  },
  {
    path: 'edit/:hash',
    component: QuestionActionComponent,
  },
  {
    path: 'detail/:hash',
    component: QuestionDetailComponent,
  }
];

@NgModule({
  declarations: [QuestionDetailComponent, QuestionActionComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
  ]
})
export class QuestionsModule { }
