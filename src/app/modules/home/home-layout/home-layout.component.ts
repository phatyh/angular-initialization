import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { distinctUntilChanged, tap } from 'rxjs/operators';
import { AuthService } from 'src/app/data/services';
import { AuthState, LogoutRequest, selectAuthState } from 'src/app/data/store/auth';

@Component({
  selector: 'app-home-layout',
  templateUrl: './home-layout.component.html',
  styleUrls: ['./home-layout.component.scss']
})
export class HomeLayoutComponent implements OnInit {
  getState$: Observable<AuthState> = this.store.select(selectAuthState);

  constructor(
    private authService: AuthService,
    private store: Store<AuthState>,
  ) { }

  ngOnInit(): void { }

  onLog(): void {
    console.log(this.authService.getTokenExpirationDate());
    const date: any = this.authService.getTokenExpirationDate();
    console.log(new Date(date).getTime());
    console.log(new Date());
    console.log(new Date().getTime());
    console.log(this.authService.isLogged());

    // console.log(this.authService.getUser());
  }

  onLogout(): void {
    this.store.dispatch(LogoutRequest());
  }

}
